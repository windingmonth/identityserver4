using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace authwebapi.Controllers
{
    [Route("identity")]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        private readonly ILogger<IdentityController> _logger;

        public IdentityController(ILogger<IdentityController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }


    [Route("Account/Login")]
    public class AccountLoginController : ControllerBase
    {
        private readonly ILogger<AccountLoginController> _logger;
        private readonly UserStore _userStore;

        public AccountLoginController(ILogger<AccountLoginController> logger, UserStore userStore)
        {
            _logger = logger;
            _userStore = userStore;
        }

        [HttpGet]
        public async Task<HttpResponse> Get()
        {
            await Response.WriteHtmlAsync(async res =>
            {
                await res.WriteAsync($"<form method=\"post\">");
                await res.WriteAsync($"<input type=\"hidden\" name=\"returnUrl\" value=\"{HttpResponseExtensions.HtmlEncode(Request.Query["ReturnUrl"])}\"/>");
                await res.WriteAsync($"<div class=\"form-group\"><label>用户名：<input type=\"text\" name=\"userName\" class=\"form-control\"></label></div>");
                await res.WriteAsync($"<div class=\"form-group\"><label>密码：<input type=\"password\" name=\"password\" class=\"form-control\"></label></div>");
                await res.WriteAsync($"<button type=\"submit\" class=\"btn btn-default\">登录</button>");
                await res.WriteAsync($"</form>");
            });

            return Response;
        }

        [HttpPost]
        public async Task<RedirectResult> Post()
        {
            var user = _userStore.FindUser(Request.Form["userName"], Request.Form["password"]);
            if (user == null)
            {
                await Response.WriteHtmlAsync(async res =>
                {
                    await res.WriteAsync($"<h1>用户名或密码错误。</h1>");
                    await res.WriteAsync("<a class=\"btn btn-default\" href=\"/Account/Login\">返回</a>");
                });

                return new RedirectResult("/");
            }
            else
            {
                var claimIdentity = new ClaimsIdentity("Cookie");
                claimIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
                claimIdentity.AddClaim(new Claim(ClaimTypes.Name, user.Name));
                claimIdentity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
                claimIdentity.AddClaim(new Claim(ClaimTypes.MobilePhone, user.PhoneNumber));
                claimIdentity.AddClaim(new Claim(ClaimTypes.DateOfBirth, user.Birthday.ToString()));

                var claimsPrincipal = new ClaimsPrincipal(claimIdentity);
                // 在上面注册AddAuthentication时，指定了默认的Scheme，在这里便可以不再指定Scheme。
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);
                var url = string.IsNullOrEmpty(Request.Form["ReturnUrl"]) ? "/" : Request.Form["ReturnUrl"][0];
                return new RedirectResult("/WeatherForecast");
            }
        }
    }

}