using authwebapi;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddSingleton<UserStore>();

builder.Services.AddControllers();

//builder.Services.AddAuthentication(options =>
//    {
//        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
//    })
//    .AddCookie(options =>
//    {
//        // 在这里可以根据需要添加一些Cookie认证相关的配置，在本次示例中使用默认值就可以了。
//    });

//AddAuthentication 将身份认证服务添加到DI，并将“Bearer”配置为默认方案。
//AddJwtBearer 将 JWT 认证处理程序添加到DI中以供身份认证服务使用
builder.Services.AddAuthentication("Bearer")
            .AddCookie()
            .AddJwtBearer("Bearer", options =>
            {
                options.Authority = "https://localhost:5000";
                options.RequireHttpsMetadata = false;

                options.Audience = "api1";


            });

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

//app.UseCookiePolicy();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();