using dotnetdemo.MyMiddleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDemoMiddlewareOptions(options => { options.flag = true; options.Version = "Services ioc Option"; });

var app = builder.Build();

// Configure the HTTP request pipeline.

app.AddDemo();

app.UseAuthorization();

app.MapControllers();

app.Run();
