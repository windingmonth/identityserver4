﻿using Microsoft.Extensions.Options;

namespace dotnetdemo.MyMiddleware
{

    public static class ServiceCoolectionExtension 
    {
        public static IServiceCollection AddDemoMiddlewareOptions(this IServiceCollection services, Action<DemoMiddlewareOptions> action) 
        {
            //通过ioc注入option
            services.Configure(action);
            return services;
        }
    }

    public static class ApplicationBuilderExtension
    {
        public static IApplicationBuilder AddDemo(this IApplicationBuilder app, Action<DemoMiddlewareOptions> action) 
        {
            DemoMiddlewareOptions op = new DemoMiddlewareOptions();
            action(op);

            app.UseMiddleware<DemoMiddleware>(Options.Create(op));
            return app; 
        }

        public static IApplicationBuilder AddDemo(this IApplicationBuilder app)
        {
            app.UseMiddleware<DemoMiddleware>();
            return app;
        }
    }

    public class DemoMiddleware
    {
        public RequestDelegate _next;
        public IOptions<DemoMiddlewareOptions> _options;
        public DemoMiddleware(RequestDelegate next, IOptions<DemoMiddlewareOptions> options)
        {
            _next = next;
            _options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            if (_options.Value.flag == true)
            {
                Console.WriteLine($"DemoMiddleware Invoke......{_options.Value.Version}");
            }
            else 
            {
                Console.WriteLine($"DemoMiddleware Invoke......no version");
            }

            await _next(context);   //继续调用下一个组件

        }
    }
}
