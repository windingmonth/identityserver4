﻿// discover endpoints from metadata
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

Console.WriteLine("========================================客户端认证========================================");
var client = new HttpClient();
var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5000");
if (disco.IsError)
{
    Console.WriteLine(disco.Error);
    return;
}

// request token
var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
{
    Address = disco.TokenEndpoint,

    ClientId = "client",
    ClientSecret = "secret",
    Scope = "api1"
});

if (tokenResponse.IsError)
{
    Console.WriteLine(tokenResponse.Error);
    return;
}

Console.WriteLine(tokenResponse.Json);


// call api
client.SetBearerToken(tokenResponse.AccessToken);

var response = await client.GetAsync("https://localhost:5001/identity");
if (!response.IsSuccessStatusCode)
{
    Console.WriteLine(response.StatusCode);
}
else
{
    var content = await response.Content.ReadAsStringAsync();
    Console.WriteLine(JArray.Parse(content));
}


Console.WriteLine("========================================用户名密码认证========================================");
//资源所有者密码认证
// request token
var passwardClient = new HttpClient();
var passwardTokenResponse = await passwardClient.RequestPasswordTokenAsync(new PasswordTokenRequest
{
    Address = disco.TokenEndpoint,
    ClientId = "ro.client",
    ClientSecret = "secret",

    UserName = "alice",
    Password = "password",
    Scope = "api1 profile"
});

if (passwardTokenResponse.IsError)
{
    Console.WriteLine(passwardTokenResponse.Error);
    return;
}

Console.WriteLine(passwardTokenResponse.Json);


// call api
passwardClient.SetBearerToken(passwardTokenResponse.AccessToken);

var passwardResponse = await passwardClient.GetAsync("https://localhost:5001/identity");
if (!passwardResponse.IsSuccessStatusCode)
{
    Console.WriteLine(passwardResponse.StatusCode);
}
else
{
    var content = await passwardResponse.Content.ReadAsStringAsync();
    Console.WriteLine(JArray.Parse(content));
}


Console.WriteLine("========================================认证========================================");

Console.ReadKey();



